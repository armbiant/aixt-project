# Supported devices and boards

## Microchip
- **8-bit 16F family**
    - PIC16F84A
    - PIC16F628A
    - PIC16F676
    - PIC16F873A
    - PIC16F886
- **8-bit 18F family**
    - PIC18F452
    - PIC18F2550
- **16-bit**
    - **_Explorer 16_**: PIC24FJ128GA010
    - **_Explorer 16_**: dsPIC33FJ256GP710A **_soon....._**

## Cypress
- **PSoC 1**
    - CY8C27443-24PXI
    - CY8C29466-24PXI
    - **_CY3209-ExpressEVK_**: CY8C21434-24LFX  **_soon....._**
    - **_CY3209-ExpressEVK_**: CY8C27643-24LFXI **_soon....._**
    - **_CY3209-ExpressEVK_**: CY8C24894-24LFXI **_soon....._**
    - **_CY3209-ExpressEVK_**: CY8C29666-24LFXI **_soon....._**
    - **_CY8CKIT-001 + (008)_**: CY8C29000-24AXI **_soon....._**
- **PSoC 3**
    - **_CY8CKIT-001 + (009)_**: CY8C38866AXI-040 **_soon....._**
- **PSoC 4**
  - **_CY8CKIT-049-42xx_**: CY8C4245AXI-483 **_soon....._**
- **PSoC 5LP**
    - **_CY8CKIT-059_**: CY8C5888LTI-LP097 **_soon....._**

## AVR
- **_MH-Tiny_**: ATtiny88 **_soon....._** 
- **_Arduino nano_**: ATmega328p

## LogicGreen 
- **_LQFP32 MiniEVB_**: lgt8f328p **_soon....._**

## Espressif
- **ESP8266**
    - **_NodeMCU V3 Lua_**: ESP8266 **_soon....._**
- **ESP32**
    - **_ESP32 DEVKITV1_**: ESP32 **_soon....._**
    - **_D1 R32_**: ESP32 **_soon....._**
    - **_CORE-ESP32_**: ESP32-C3 **_soon....._**

## ST
- **_Blue Pill_**: STM32F103C6 **_soon....._**

## LuatOS
- **_CORE-Air32F103CBT6_**: air32f103 **_soon....._**

## Raspberry Pi
- **_Raspberry Pi Pico_**: RP2040 **_soon....._**
- **_Raspberry Pi Pico W_**: RP2040 **_soon....._**

## WCH
- **_CH552 Core Board_**: CH552 **_soon....._**

## WinnerMicro
- **_HLK-W801-KIT-V1.1_**: W801-C400 **_soon....._**

## LEGO Mindstorms NXT
- **_NXT Intelligent Brick_**: AT91SAM7S256 (_NXC_ Language)