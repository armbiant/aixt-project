# **_Aixt_** API folder tree
All the API implementations follow this basic folder tree:

```
API/
 ├── setup.h
 ├── machine.h
 ├── machine/
 │    ├── pin.h
 │    ├── adc.h
 │    └── uart.h
 ├── time.h
 └── time/
      ├── sleep_ms.h
      └── sleep_us.h
```