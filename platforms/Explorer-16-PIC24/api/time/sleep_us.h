// This file is part of the Aixt project, https://gitlab.com/fermarsan/aixt-project.git
//
// The MIT License (MIT)
// 
// Copyright (c) 2022 Fernando Martínez Santa

#ifndef _SLEEP_US_H_
#define _SLEEP_US_H_

#define sleep_us(TIME)    __delay_us(TIME)

#endif  //_SLEEP_US_H_